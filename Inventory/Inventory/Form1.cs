﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kyro.inventory.form
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void newPurchaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPurchaseOrder form = new FormPurchaseOrder();
            form.MdiParent = this;
            form.Show();
        }

        private void updatePurchaeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void purchaseDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPurchases formPurchases = new FormPurchases();
            formPurchases.MdiParent = this;
            formPurchases.Show();
        }

        private void newSalesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSales formSales = new FormSales();
            formSales.MdiParent = this;
            formSales.Show();
        }
    }
}
