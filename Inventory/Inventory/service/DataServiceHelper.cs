﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Globalization;

namespace kyro.inventory.service
{
    public static class DataServiceHelper
    {
        public static string baseRestUrl = "http://localhost:8080/api/";

        public static HttpClient getHttpClient()
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("http://localhost:8080/api");
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }

        public static string numberToString(double number)
        {
            CultureInfo ci = new CultureInfo("id-ID");
            NumberFormatInfo nfi = ci.NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();
            nfi.CurrencySymbol = "";

            String ret = string.Format(nfi, "{0:c2}", number);
            return ret;
        }

        public static double stringToNumber(string number)
        {
            CultureInfo ci = new CultureInfo("id-ID");
            NumberFormatInfo nfi = ci.NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();
            nfi.CurrencySymbol = "";

            double ret = Double.Parse(number, nfi);
            return ret;
        }

        public static bool tryParseNumber(string number)
        {
            CultureInfo ci = new CultureInfo("id-ID");
            NumberFormatInfo nfi = ci.NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();
            nfi.CurrencySymbol = "";

            double value = 0;
            bool ret = Double.TryParse(number,NumberStyles.Number,nfi,out value);
            return ret;
        }

    }
}
