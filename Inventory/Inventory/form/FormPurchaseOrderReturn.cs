﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using kyro.inventory.model;
using kyro.inventory.service;
using Newtonsoft.Json;
using System.Reflection;

namespace kyro.inventory.form
{
    public partial class FormPurchaseOrderReturn : Form
    {
        BindingSource bindingSource;

        public long purchaseId;
        public Purchase purchase;
        public List<OrderDetails> orderDetailsList = new List<OrderDetails>();
        public List<ReturnDetails> returnDetailsList = new List<ReturnDetails>();
        public List<OrderDetails> orderDetailsUpdate = new List<OrderDetails>();

        public FormPurchaseOrderReturn(long purchaseId)
        {
            this.purchaseId = purchaseId;

            InitializeComponent();

            this.tableOrderDetails.Columns["colQuantityUOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void FormPurchaseOrder_Load(object sender, EventArgs e)
        {
            dtDate.Enabled = false;
            dtDueDate.Enabled = false;

            CalendarColumn colDate = new CalendarColumn();
            tableOrderDetails.Columns.Add(colDate);
            foreach (DataGridViewRow row in this.tableOrderDetails.Rows)
            {
                row.Cells[4].Value = DateTime.Now;
            }
            colDate.HeaderText = "Return Date";
            colDate.DataPropertyName = "returnDate";

            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage responseGetPurchase = httpClient.GetAsync(DataServiceHelper.baseRestUrl + "/purchaseOrders/" + this.purchaseId).Result;

            this.purchase = responseGetPurchase.Content.ReadAsAsync<Purchase>().Result;
            this.orderDetailsList = this.purchase.orders;
            getReturnDetailsOfOrder();

            txtNo.Text = this.purchase.no;
            txtVendor.Text = this.purchase.vendor.name;

            var gridBindingList = new BindingList<ReturnDetails>(this.returnDetailsList);
            tableOrderDetails.AutoGenerateColumns = false;
            tableOrderDetails.DataSource = gridBindingList;
        }

        private void getReturnDetailsOfOrder()
        {
            foreach(OrderDetails orderDetails in this.purchase.orders)
            {
                ReturnDetails returnDetails = new ReturnDetails
                {
                    orderDetails = orderDetails,
                    id = orderDetails.returnDetails != null ? orderDetails.returnDetails.id : null,
                    purchaseId = (long)this.purchase.id,
                    product = orderDetails.product,
                    quantityUOM = orderDetails.returnDetails != null ? orderDetails.returnDetails.quantityUOM : 0,
                    quantityUOMStr = orderDetails.returnDetails != null ?
                        DataServiceHelper.numberToString(orderDetails.returnDetails.quantityUOM) : "0",
                    orderQtyStr = DataServiceHelper.numberToString(orderDetails.quantityUOM),
                    usePurchaseUOM = orderDetails.usePurchaseUOM,
                    purchaseUOMConversion = orderDetails.purchaseUOMConversion,
                    returnDate = orderDetails.returnDetails != null ? orderDetails.returnDetails.returnDate : DateTime.Now
                };

                this.returnDetailsList.Add(returnDetails);
            }

        }

        private void setReturnDetails()
        {
            foreach(ReturnDetails returnDetails in this.returnDetailsList)
            {

                if(
                    (
                    returnDetails.quantityUOM!=null && 
                    returnDetails.quantityUOM > 0
                    ) ||
                    (
                        returnDetails.id != null &&
                        returnDetails.quantityUOM != null 
                    )
                  )
                {
                    returnDetails.quantityUOM = DataServiceHelper.stringToNumber(returnDetails.quantityUOMStr);
                    double quantity = returnDetails.quantityUOM * returnDetails.purchaseUOMConversion;

                    returnDetails.quantity = quantity;

                    OrderDetails orderDetails = returnDetails.orderDetails;
                    orderDetails.returnDetails = returnDetails;
                    
                    this.orderDetailsUpdate.Add(orderDetails);
                }
            }
        }
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            setReturnDetails();
            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage response = httpClient.PutAsJsonAsync(DataServiceHelper.baseRestUrl + "purchaseReturns/" + purchase.id, this.orderDetailsUpdate).Result;
            
            var result = MessageBox.Show("Receive update success!", "Success",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                Close();
            }
        }

        private void tableOrderDetails_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView grid = (DataGridView)sender;
            DataGridViewRow row = grid.Rows[e.RowIndex];
            DataGridViewColumn col = grid.Columns[e.ColumnIndex];

            if (row.DataBoundItem != null && col.DataPropertyName.Contains(".") && col.Index == 0)
            {
                string[] props = col.DataPropertyName.Split('.');
                PropertyInfo propInfo = row.DataBoundItem.GetType().GetProperty(props[0]);
                object val = propInfo.GetValue(row.DataBoundItem, null);
                for (int i = 1; i < props.Length; i++)
                {
                    propInfo = val.GetType().GetProperty(props[i]);
                    val = propInfo.GetValue(val, null);
                }
                e.Value = val;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
