﻿namespace kyro.inventory.form
{
    partial class FormPurchases
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBoxFilter = new System.Windows.Forms.GroupBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.dtDueDateTo = new System.Windows.Forms.DateTimePicker();
            this.ckDueDateTo = new System.Windows.Forms.CheckBox();
            this.lbDueDateTo = new System.Windows.Forms.Label();
            this.dtDueDateForm = new System.Windows.Forms.DateTimePicker();
            this.ckDueDateFrom = new System.Windows.Forms.CheckBox();
            this.lbDueDateFrom = new System.Windows.Forms.Label();
            this.ckDateTo = new System.Windows.Forms.CheckBox();
            this.dtDateTo = new System.Windows.Forms.DateTimePicker();
            this.ckDateFrom = new System.Windows.Forms.CheckBox();
            this.cbProduct = new System.Windows.Forms.ComboBox();
            this.lbProduct = new System.Windows.Forms.Label();
            this.cbVendor = new System.Windows.Forms.ComboBox();
            this.lbDateTo = new System.Windows.Forms.Label();
            this.dtDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lbDateFrom = new System.Windows.Forms.Label();
            this.lbVendor = new System.Windows.Forms.Label();
            this.lbTitle = new System.Windows.Forms.Label();
            this.tablePurchase = new System.Windows.Forms.DataGridView();
            this.noColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vendorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dueDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.editColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.receiveColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.returnColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBoxFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablePurchase)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxFilter
            // 
            this.groupBoxFilter.Controls.Add(this.btnFilter);
            this.groupBoxFilter.Controls.Add(this.dtDueDateTo);
            this.groupBoxFilter.Controls.Add(this.ckDueDateTo);
            this.groupBoxFilter.Controls.Add(this.lbDueDateTo);
            this.groupBoxFilter.Controls.Add(this.dtDueDateForm);
            this.groupBoxFilter.Controls.Add(this.ckDueDateFrom);
            this.groupBoxFilter.Controls.Add(this.lbDueDateFrom);
            this.groupBoxFilter.Controls.Add(this.ckDateTo);
            this.groupBoxFilter.Controls.Add(this.dtDateTo);
            this.groupBoxFilter.Controls.Add(this.ckDateFrom);
            this.groupBoxFilter.Controls.Add(this.cbProduct);
            this.groupBoxFilter.Controls.Add(this.lbProduct);
            this.groupBoxFilter.Controls.Add(this.cbVendor);
            this.groupBoxFilter.Controls.Add(this.lbDateTo);
            this.groupBoxFilter.Controls.Add(this.dtDateFrom);
            this.groupBoxFilter.Controls.Add(this.lbDateFrom);
            this.groupBoxFilter.Controls.Add(this.lbVendor);
            this.groupBoxFilter.Location = new System.Drawing.Point(12, 60);
            this.groupBoxFilter.Name = "groupBoxFilter";
            this.groupBoxFilter.Size = new System.Drawing.Size(984, 120);
            this.groupBoxFilter.TabIndex = 0;
            this.groupBoxFilter.TabStop = false;
            this.groupBoxFilter.Text = "Filter";
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(892, 20);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 23);
            this.btnFilter.TabIndex = 17;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            // 
            // dtDueDateTo
            // 
            this.dtDueDateTo.Location = new System.Drawing.Point(523, 75);
            this.dtDueDateTo.Name = "dtDueDateTo";
            this.dtDueDateTo.Size = new System.Drawing.Size(206, 20);
            this.dtDueDateTo.TabIndex = 16;
            // 
            // ckDueDateTo
            // 
            this.ckDueDateTo.AutoSize = true;
            this.ckDueDateTo.Location = new System.Drawing.Point(502, 78);
            this.ckDueDateTo.Name = "ckDueDateTo";
            this.ckDueDateTo.Size = new System.Drawing.Size(15, 14);
            this.ckDueDateTo.TabIndex = 15;
            this.ckDueDateTo.UseVisualStyleBackColor = true;
            // 
            // lbDueDateTo
            // 
            this.lbDueDateTo.AutoSize = true;
            this.lbDueDateTo.Location = new System.Drawing.Point(419, 79);
            this.lbDueDateTo.Name = "lbDueDateTo";
            this.lbDueDateTo.Size = new System.Drawing.Size(69, 13);
            this.lbDueDateTo.TabIndex = 14;
            this.lbDueDateTo.Text = "Due Date To";
            // 
            // dtDueDateForm
            // 
            this.dtDueDateForm.Location = new System.Drawing.Point(140, 75);
            this.dtDueDateForm.Name = "dtDueDateForm";
            this.dtDueDateForm.Size = new System.Drawing.Size(226, 20);
            this.dtDueDateForm.TabIndex = 13;
            // 
            // ckDueDateFrom
            // 
            this.ckDueDateFrom.AutoSize = true;
            this.ckDueDateFrom.Location = new System.Drawing.Point(118, 80);
            this.ckDueDateFrom.Name = "ckDueDateFrom";
            this.ckDueDateFrom.Size = new System.Drawing.Size(15, 14);
            this.ckDueDateFrom.TabIndex = 12;
            this.ckDueDateFrom.UseVisualStyleBackColor = true;
            // 
            // lbDueDateFrom
            // 
            this.lbDueDateFrom.AutoSize = true;
            this.lbDueDateFrom.Location = new System.Drawing.Point(21, 80);
            this.lbDueDateFrom.Name = "lbDueDateFrom";
            this.lbDueDateFrom.Size = new System.Drawing.Size(79, 13);
            this.lbDueDateFrom.TabIndex = 11;
            this.lbDueDateFrom.Text = "Due Date Form";
            // 
            // ckDateTo
            // 
            this.ckDateTo.AutoSize = true;
            this.ckDateTo.Location = new System.Drawing.Point(502, 51);
            this.ckDateTo.Name = "ckDateTo";
            this.ckDateTo.Size = new System.Drawing.Size(15, 14);
            this.ckDateTo.TabIndex = 10;
            this.ckDateTo.UseVisualStyleBackColor = true;
            // 
            // dtDateTo
            // 
            this.dtDateTo.Location = new System.Drawing.Point(523, 49);
            this.dtDateTo.Name = "dtDateTo";
            this.dtDateTo.Size = new System.Drawing.Size(207, 20);
            this.dtDateTo.TabIndex = 9;
            // 
            // ckDateFrom
            // 
            this.ckDateFrom.AutoSize = true;
            this.ckDateFrom.Location = new System.Drawing.Point(118, 55);
            this.ckDateFrom.Name = "ckDateFrom";
            this.ckDateFrom.Size = new System.Drawing.Size(15, 14);
            this.ckDateFrom.TabIndex = 8;
            this.ckDateFrom.UseVisualStyleBackColor = true;
            // 
            // cbProduct
            // 
            this.cbProduct.FormattingEnabled = true;
            this.cbProduct.Location = new System.Drawing.Point(523, 22);
            this.cbProduct.Name = "cbProduct";
            this.cbProduct.Size = new System.Drawing.Size(207, 21);
            this.cbProduct.TabIndex = 7;
            // 
            // lbProduct
            // 
            this.lbProduct.AutoSize = true;
            this.lbProduct.Location = new System.Drawing.Point(419, 25);
            this.lbProduct.Name = "lbProduct";
            this.lbProduct.Size = new System.Drawing.Size(44, 13);
            this.lbProduct.TabIndex = 6;
            this.lbProduct.Text = "Product";
            // 
            // cbVendor
            // 
            this.cbVendor.FormattingEnabled = true;
            this.cbVendor.Location = new System.Drawing.Point(139, 22);
            this.cbVendor.Name = "cbVendor";
            this.cbVendor.Size = new System.Drawing.Size(227, 21);
            this.cbVendor.TabIndex = 5;
            // 
            // lbDateTo
            // 
            this.lbDateTo.AutoSize = true;
            this.lbDateTo.Location = new System.Drawing.Point(419, 51);
            this.lbDateTo.Name = "lbDateTo";
            this.lbDateTo.Size = new System.Drawing.Size(46, 13);
            this.lbDateTo.TabIndex = 4;
            this.lbDateTo.Text = "Date To";
            // 
            // dtDateFrom
            // 
            this.dtDateFrom.Location = new System.Drawing.Point(139, 49);
            this.dtDateFrom.Name = "dtDateFrom";
            this.dtDateFrom.Size = new System.Drawing.Size(227, 20);
            this.dtDateFrom.TabIndex = 3;
            // 
            // lbDateFrom
            // 
            this.lbDateFrom.AutoSize = true;
            this.lbDateFrom.Location = new System.Drawing.Point(19, 52);
            this.lbDateFrom.Name = "lbDateFrom";
            this.lbDateFrom.Size = new System.Drawing.Size(56, 13);
            this.lbDateFrom.TabIndex = 2;
            this.lbDateFrom.Text = "Date From";
            // 
            // lbVendor
            // 
            this.lbVendor.AutoSize = true;
            this.lbVendor.Location = new System.Drawing.Point(19, 25);
            this.lbVendor.Name = "lbVendor";
            this.lbVendor.Size = new System.Drawing.Size(41, 13);
            this.lbVendor.TabIndex = 0;
            this.lbVendor.Text = "Vendor";
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.Location = new System.Drawing.Point(9, 18);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(170, 29);
            this.lbTitle.TabIndex = 1;
            this.lbTitle.Text = "Purchase List";
            // 
            // tablePurchase
            // 
            this.tablePurchase.AllowUserToAddRows = false;
            this.tablePurchase.AllowUserToDeleteRows = false;
            this.tablePurchase.AllowUserToOrderColumns = true;
            this.tablePurchase.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tablePurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablePurchase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.noColumn,
            this.vendorColumn,
            this.dateColumn,
            this.dueDateColumn,
            this.totalColumn,
            this.editColumn,
            this.receiveColumn,
            this.returnColumn});
            this.tablePurchase.Location = new System.Drawing.Point(12, 195);
            this.tablePurchase.Name = "tablePurchase";
            this.tablePurchase.Size = new System.Drawing.Size(984, 327);
            this.tablePurchase.TabIndex = 2;
            this.tablePurchase.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tablePurchase_CellContentClick);
            this.tablePurchase.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.tablePurchase_CellFormatting);
            // 
            // noColumn
            // 
            this.noColumn.DataPropertyName = "no";
            this.noColumn.HeaderText = "Order #";
            this.noColumn.Name = "noColumn";
            this.noColumn.ReadOnly = true;
            // 
            // vendorColumn
            // 
            this.vendorColumn.DataPropertyName = "vendor.name";
            this.vendorColumn.HeaderText = "Vendor";
            this.vendorColumn.Name = "vendorColumn";
            this.vendorColumn.ReadOnly = true;
            // 
            // dateColumn
            // 
            this.dateColumn.DataPropertyName = "date";
            this.dateColumn.HeaderText = "Date";
            this.dateColumn.Name = "dateColumn";
            this.dateColumn.ReadOnly = true;
            // 
            // dueDateColumn
            // 
            this.dueDateColumn.DataPropertyName = "dueDate";
            this.dueDateColumn.HeaderText = "Due Date";
            this.dueDateColumn.Name = "dueDateColumn";
            this.dueDateColumn.ReadOnly = true;
            // 
            // totalColumn
            // 
            this.totalColumn.DataPropertyName = "total";
            this.totalColumn.HeaderText = "Total";
            this.totalColumn.Name = "totalColumn";
            this.totalColumn.ReadOnly = true;
            // 
            // editColumn
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = "Edit";
            this.editColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.editColumn.HeaderText = "Edit";
            this.editColumn.Name = "editColumn";
            this.editColumn.ReadOnly = true;
            this.editColumn.Text = "Edit";
            // 
            // receiveColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "Receive";
            this.receiveColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.receiveColumn.HeaderText = "Receive";
            this.receiveColumn.Name = "receiveColumn";
            this.receiveColumn.ReadOnly = true;
            this.receiveColumn.Text = "Receive";
            // 
            // returnColumn
            // 
            this.returnColumn.HeaderText = "Return";
            this.returnColumn.Name = "returnColumn";
            this.returnColumn.ReadOnly = true;
            // 
            // FormPurchases
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.tablePurchase);
            this.Controls.Add(this.lbTitle);
            this.Controls.Add(this.groupBoxFilter);
            this.Name = "FormPurchases";
            this.Text = "Purchase";
            this.Load += new System.EventHandler(this.FormPurchases_Load);
            this.groupBoxFilter.ResumeLayout(false);
            this.groupBoxFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablePurchase)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFilter;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.ComboBox cbProduct;
        private System.Windows.Forms.Label lbProduct;
        private System.Windows.Forms.ComboBox cbVendor;
        private System.Windows.Forms.Label lbDateTo;
        private System.Windows.Forms.DateTimePicker dtDateFrom;
        private System.Windows.Forms.Label lbDateFrom;
        private System.Windows.Forms.Label lbVendor;
        private System.Windows.Forms.CheckBox ckDateFrom;
        private System.Windows.Forms.CheckBox ckDateTo;
        private System.Windows.Forms.DateTimePicker dtDateTo;
        private System.Windows.Forms.Label lbDueDateFrom;
        private System.Windows.Forms.CheckBox ckDueDateFrom;
        private System.Windows.Forms.Label lbDueDateTo;
        private System.Windows.Forms.DateTimePicker dtDueDateForm;
        private System.Windows.Forms.CheckBox ckDueDateTo;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DateTimePicker dtDueDateTo;
        private System.Windows.Forms.DataGridView tablePurchase;
        private System.Windows.Forms.DataGridViewTextBoxColumn noColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vendorColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dueDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalColumn;
        private System.Windows.Forms.DataGridViewButtonColumn editColumn;
        private System.Windows.Forms.DataGridViewButtonColumn receiveColumn;
        private System.Windows.Forms.DataGridViewButtonColumn returnColumn;
    }
}