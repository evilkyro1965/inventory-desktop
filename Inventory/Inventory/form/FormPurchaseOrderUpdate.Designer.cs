﻿
namespace kyro.inventory.form
{
    partial class FormPurchaseOrderUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableOrderDetails = new System.Windows.Forms.DataGridView();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.vendorCombo = new System.Windows.Forms.ComboBox();
            this.dtDueDate = new System.Windows.Forms.DateTimePicker();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtTaxPercent = new System.Windows.Forms.TextBox();
            this.txtTaxTotal = new System.Windows.Forms.TextBox();
            this.txtFreight = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lbTax = new System.Windows.Forms.Label();
            this.lbTaxTotal = new System.Windows.Forms.Label();
            this.lbFreight = new System.Windows.Forms.Label();
            this.lbTotal = new System.Windows.Forms.Label();
            this.lbTitle = new System.Windows.Forms.Label();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.lbSubTotal = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.colProduct = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colMeasurement = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.deleteColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantityUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDiscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tableOrderDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vendor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Due Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(723, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(723, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Date";
            // 
            // tableOrderDetails
            // 
            this.tableOrderDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableOrderDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableOrderDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProduct,
            this.colMeasurement,
            this.colQuantityUOM,
            this.colUnitPrice,
            this.colDiscount,
            this.colSubTotal,
            this.deleteColumn});
            this.tableOrderDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.tableOrderDetails.Location = new System.Drawing.Point(12, 135);
            this.tableOrderDetails.Name = "tableOrderDetails";
            this.tableOrderDetails.Size = new System.Drawing.Size(984, 227);
            this.tableOrderDetails.TabIndex = 4;
            this.tableOrderDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableOrderDetails_CellContentClick);
            this.tableOrderDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableOrderDetails_CellValueChanged);
            this.tableOrderDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.tableOrderDetails_EditingControlShowing);
            this.tableOrderDetails.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.tableOrderDetails_RowsAdded);
            // 
            // txtNo
            // 
            this.txtNo.Location = new System.Drawing.Point(792, 67);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(200, 20);
            this.txtNo.TabIndex = 5;
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(792, 94);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(200, 20);
            this.dtDate.TabIndex = 6;
            // 
            // vendorCombo
            // 
            this.vendorCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.vendorCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.vendorCombo.DisplayMember = "name";
            this.vendorCombo.FormattingEnabled = true;
            this.vendorCombo.Location = new System.Drawing.Point(116, 67);
            this.vendorCombo.Name = "vendorCombo";
            this.vendorCombo.Size = new System.Drawing.Size(236, 21);
            this.vendorCombo.TabIndex = 7;
            this.vendorCombo.ValueMember = "id";
            // 
            // dtDueDate
            // 
            this.dtDueDate.Location = new System.Drawing.Point(116, 94);
            this.dtDueDate.Name = "dtDueDate";
            this.dtDueDate.Size = new System.Drawing.Size(236, 20);
            this.dtDueDate.TabIndex = 8;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(839, 526);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtTaxPercent
            // 
            this.txtTaxPercent.Location = new System.Drawing.Point(792, 408);
            this.txtTaxPercent.Name = "txtTaxPercent";
            this.txtTaxPercent.Size = new System.Drawing.Size(203, 20);
            this.txtTaxPercent.TabIndex = 10;
            this.txtTaxPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxPercent.TextChanged += new System.EventHandler(this.txtTotal_TextChanged);
            this.txtTaxPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotal_KeyPress);
            // 
            // txtTaxTotal
            // 
            this.txtTaxTotal.Location = new System.Drawing.Point(792, 434);
            this.txtTaxTotal.Name = "txtTaxTotal";
            this.txtTaxTotal.ReadOnly = true;
            this.txtTaxTotal.Size = new System.Drawing.Size(203, 20);
            this.txtTaxTotal.TabIndex = 11;
            this.txtTaxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtFreight
            // 
            this.txtFreight.Location = new System.Drawing.Point(792, 460);
            this.txtFreight.Name = "txtFreight";
            this.txtFreight.Size = new System.Drawing.Size(203, 20);
            this.txtFreight.TabIndex = 12;
            this.txtFreight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFreight.TextChanged += new System.EventHandler(this.txtTotal_TextChanged);
            this.txtFreight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotal_KeyPress);
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(792, 486);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(203, 20);
            this.txtTotal.TabIndex = 13;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbTax
            // 
            this.lbTax.AutoSize = true;
            this.lbTax.Location = new System.Drawing.Point(723, 411);
            this.lbTax.Name = "lbTax";
            this.lbTax.Size = new System.Drawing.Size(65, 13);
            this.lbTax.TabIndex = 14;
            this.lbTax.Text = "Tax Percent";
            // 
            // lbTaxTotal
            // 
            this.lbTaxTotal.AutoSize = true;
            this.lbTaxTotal.Location = new System.Drawing.Point(723, 437);
            this.lbTaxTotal.Name = "lbTaxTotal";
            this.lbTaxTotal.Size = new System.Drawing.Size(52, 13);
            this.lbTaxTotal.TabIndex = 15;
            this.lbTaxTotal.Text = "Tax Total";
            // 
            // lbFreight
            // 
            this.lbFreight.AutoSize = true;
            this.lbFreight.Location = new System.Drawing.Point(723, 463);
            this.lbFreight.Name = "lbFreight";
            this.lbFreight.Size = new System.Drawing.Size(39, 13);
            this.lbFreight.TabIndex = 16;
            this.lbFreight.Text = "Freight";
            // 
            // lbTotal
            // 
            this.lbTotal.AutoSize = true;
            this.lbTotal.Location = new System.Drawing.Point(723, 489);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(31, 13);
            this.lbTotal.TabIndex = 17;
            this.lbTotal.Text = "Total";
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.Location = new System.Drawing.Point(7, 18);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(197, 29);
            this.lbTitle.TabIndex = 18;
            this.lbTitle.Text = "Purchase Order";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.Location = new System.Drawing.Point(792, 382);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(204, 20);
            this.txtSubTotal.TabIndex = 19;
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbSubTotal
            // 
            this.lbSubTotal.AutoSize = true;
            this.lbSubTotal.Location = new System.Drawing.Point(723, 385);
            this.lbSubTotal.Name = "lbSubTotal";
            this.lbSubTotal.Size = new System.Drawing.Size(53, 13);
            this.lbSubTotal.TabIndex = 20;
            this.lbSubTotal.Text = "Sub Total";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(920, 526);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // colProduct
            // 
            this.colProduct.DataPropertyName = "productId";
            this.colProduct.HeaderText = "Product";
            this.colProduct.Name = "colProduct";
            // 
            // colMeasurement
            // 
            this.colMeasurement.HeaderText = "Measurement";
            this.colMeasurement.Name = "colMeasurement";
            // 
            // deleteColumn
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = "Delete";
            this.deleteColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.deleteColumn.HeaderText = "Delete";
            this.deleteColumn.Name = "deleteColumn";
            this.deleteColumn.Text = "Delete";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "quantityUOMStr";
            this.dataGridViewTextBoxColumn1.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 140;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "unitPriceStr";
            this.dataGridViewTextBoxColumn2.HeaderText = "Unit Price";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "discountStr";
            this.dataGridViewTextBoxColumn3.HeaderText = "Discount";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "subTotalStr";
            this.dataGridViewTextBoxColumn4.HeaderText = "Sub Total";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // colQuantityUOM
            // 
            this.colQuantityUOM.DataPropertyName = "quantityUOMStr";
            this.colQuantityUOM.HeaderText = "Quantity";
            this.colQuantityUOM.Name = "colQuantityUOM";
            // 
            // colUnitPrice
            // 
            this.colUnitPrice.DataPropertyName = "unitPriceStr";
            this.colUnitPrice.HeaderText = "Unit Price";
            this.colUnitPrice.Name = "colUnitPrice";
            // 
            // colDiscount
            // 
            this.colDiscount.DataPropertyName = "discountStr";
            this.colDiscount.HeaderText = "Discount";
            this.colDiscount.Name = "colDiscount";
            // 
            // colSubTotal
            // 
            this.colSubTotal.DataPropertyName = "subTotalStr";
            this.colSubTotal.HeaderText = "Sub Total";
            this.colSubTotal.Name = "colSubTotal";
            this.colSubTotal.ReadOnly = true;
            // 
            // FormPurchaseOrderUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lbSubTotal);
            this.Controls.Add(this.txtSubTotal);
            this.Controls.Add(this.lbTitle);
            this.Controls.Add(this.lbTotal);
            this.Controls.Add(this.lbFreight);
            this.Controls.Add(this.lbTaxTotal);
            this.Controls.Add(this.lbTax);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.txtFreight);
            this.Controls.Add(this.txtTaxTotal);
            this.Controls.Add(this.txtTaxPercent);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dtDueDate);
            this.Controls.Add(this.vendorCombo);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.txtNo);
            this.Controls.Add(this.tableOrderDetails);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Name = "FormPurchaseOrderUpdate";
            this.Text = "Purchase Order";
            this.Load += new System.EventHandler(this.FormPurchaseOrder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tableOrderDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView tableOrderDetails;
        private System.Windows.Forms.TextBox txtNo;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.ComboBox vendorCombo;
        private System.Windows.Forms.DateTimePicker dtDueDate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtTaxPercent;
        private System.Windows.Forms.TextBox txtTaxTotal;
        private System.Windows.Forms.TextBox txtFreight;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lbTax;
        private System.Windows.Forms.Label lbTaxTotal;
        private System.Windows.Forms.Label lbFreight;
        private System.Windows.Forms.Label lbTotal;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.Label lbSubTotal;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewComboBoxColumn colProduct;
        private System.Windows.Forms.DataGridViewComboBoxColumn colMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantityUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUnitPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDiscount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSubTotal;
        private System.Windows.Forms.DataGridViewButtonColumn deleteColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    }
}