﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using kyro.inventory.model;
using kyro.inventory.service;
using Newtonsoft.Json;
using System.Reflection;

namespace kyro.inventory.form
{
    public partial class FormPurchaseOrderReceive : Form
    {
        BindingSource bindingSource;

        public long purchaseId;
        public List<Location> locations;
        public Purchase purchase;
        public List<OrderDetails> orderDetailsList = new List<OrderDetails>();
        public List<ReceiveDetails> receiveDetailsList = new List<ReceiveDetails>();
        public List<OrderDetails> orderDetailsUpdate = new List<OrderDetails>();

        public FormPurchaseOrderReceive(long purchaseId)
        {
            this.purchaseId = purchaseId;

            InitializeComponent();

            this.tableOrderDetails.Columns["colQuantityUOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void FormPurchaseOrder_Load(object sender, EventArgs e)
        {
            dtDate.Enabled = false;
            dtDueDate.Enabled = false;

            CalendarColumn colDate = new CalendarColumn();
            tableOrderDetails.Columns.Add(colDate);
            foreach (DataGridViewRow row in this.tableOrderDetails.Rows)
            {
                row.Cells[4].Value = DateTime.Now;
            }
            colDate.HeaderText = "Receive Date";
            colDate.DataPropertyName = "receiveDate";

            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage responseGetLocation = httpClient.GetAsync(DataServiceHelper.baseRestUrl + "/locations/").Result;
            HttpResponseMessage responseGetPurchase = httpClient.GetAsync(DataServiceHelper.baseRestUrl + "/purchaseOrders/" + this.purchaseId).Result;

            var locations = responseGetLocation.Content.ReadAsAsync<IEnumerable<Location>>().Result;
            this.locations = locations.ToList();

            DataGridViewComboBoxColumn colLocation = (DataGridViewComboBoxColumn)this.tableOrderDetails.Columns[1];
            colLocation.DisplayMember = "name";
            colLocation.ValueMember = "id";
            colLocation.DataPropertyName = "locationId";
            colLocation.DataSource = this.locations;

            this.purchase = responseGetPurchase.Content.ReadAsAsync<Purchase>().Result;
            this.orderDetailsList = this.purchase.orders;
            getReceiveDetailsOfOrder();

            txtNo.Text = this.purchase.no;
            txtVendor.Text = this.purchase.vendor.name;

            var gridBindingList = new BindingList<ReceiveDetails>(this.receiveDetailsList);
            tableOrderDetails.AutoGenerateColumns = false;
            tableOrderDetails.DataSource = gridBindingList;
        }

        private void getReceiveDetailsOfOrder()
        {
            foreach(OrderDetails orderDetails in this.purchase.orders)
            {
                ReceiveDetails receiveDetails = new ReceiveDetails
                {
                    orderDetails = orderDetails,
                    id = orderDetails.receiveDetails != null ? orderDetails.receiveDetails.id : null,
                    purchaseId = (long)this.purchase.id,
                    product = orderDetails.product,
                    locationId = orderDetails.receiveDetails != null && orderDetails.receiveDetails.location != null ?
                        orderDetails.receiveDetails.location.id : null,
                    quantityUOM = orderDetails.receiveDetails != null ? orderDetails.receiveDetails.quantityUOM : 0,
                    quantityUOMStr = orderDetails.receiveDetails != null ?
                        DataServiceHelper.numberToString(orderDetails.receiveDetails.quantityUOM) : "0",
                    orderQtyStr = DataServiceHelper.numberToString(orderDetails.quantityUOM),
                    usePurchaseUOM = orderDetails.usePurchaseUOM,
                    purchaseUOMConversion = orderDetails.purchaseUOMConversion,
                    receiveDate = orderDetails.receiveDetails != null ? orderDetails.receiveDetails.receiveDate : DateTime.Now
                };

                this.receiveDetailsList.Add(receiveDetails);
            }

        }

        private void setReceiveDetails()
        {
            foreach(ReceiveDetails receiveDetails in this.receiveDetailsList)
            {

                if(
                    receiveDetails != null &&
                    receiveDetails.locationId!=null && 
                    receiveDetails.quantityUOM!=null)
                {
                    receiveDetails.quantityUOM = DataServiceHelper.stringToNumber(receiveDetails.quantityUOMStr);
                    double quantity = receiveDetails.quantityUOM * receiveDetails.purchaseUOMConversion;
                    Location location = new Location { id = receiveDetails.locationId };

                    receiveDetails.quantity = quantity;
                    receiveDetails.location = location;

                    OrderDetails orderDetails = receiveDetails.orderDetails;
                    orderDetails.receiveDetails = receiveDetails;
                    
                    this.orderDetailsUpdate.Add(orderDetails);
                }
            }
        }
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            setReceiveDetails();
            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage response = httpClient.PutAsJsonAsync(DataServiceHelper.baseRestUrl + "purchaseReceives/" + purchase.id, this.orderDetailsUpdate).Result;
            
            var result = MessageBox.Show("Receive update success!", "Success",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                Close();
            }
        }

        private void tableOrderDetails_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView grid = (DataGridView)sender;
            DataGridViewRow row = grid.Rows[e.RowIndex];
            DataGridViewColumn col = grid.Columns[e.ColumnIndex];

            if (row.DataBoundItem != null && col.DataPropertyName.Contains(".") && col.Index == 0)
            {
                string[] props = col.DataPropertyName.Split('.');
                PropertyInfo propInfo = row.DataBoundItem.GetType().GetProperty(props[0]);
                object val = propInfo.GetValue(row.DataBoundItem, null);
                for (int i = 1; i < props.Length; i++)
                {
                    propInfo = val.GetType().GetProperty(props[i]);
                    val = propInfo.GetValue(val, null);
                }
                e.Value = val;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
