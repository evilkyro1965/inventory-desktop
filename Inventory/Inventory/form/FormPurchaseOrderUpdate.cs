﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using kyro.inventory.model;
using kyro.inventory.service;
using Newtonsoft.Json;

namespace kyro.inventory.form
{
    public partial class FormPurchaseOrderUpdate : Form
    {
        BindingSource bindingSource;

        public long purchaseId;
        public Purchase purchase;
        public List<OrderDetails> orderDetailsOri = new List<OrderDetails>();
        public List<OrderDetails> orderDetailsList = new List<OrderDetails>();
        public List<Product> products;
        public double subTotals = 0;
        public string subTotalsStr = "0";
        public double taxPercent = 0;
        public string taxPercentStr = "0";
        public double taxTotal = 0;
        public string taxTotalStr = "0";
        public double freight = 0;
        public string freightStr = "0";
        public double total = 0;
        public string totalStr = "0";

        public FormPurchaseOrderUpdate(long purchaseId)
        {
            this.purchaseId = purchaseId;

            InitializeComponent();

            this.tableOrderDetails.Columns["colQuantityUOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.tableOrderDetails.Columns["colUnitPrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.tableOrderDetails.Columns["colDiscount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.tableOrderDetails.Columns["colSubTotal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            txtFreight.Text = "0";
            txtTaxPercent.Text = "0";
        }

        private void FormPurchaseOrder_Load(object sender, EventArgs e)
        {

            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage responseGetVendors = httpClient.GetAsync(DataServiceHelper.baseRestUrl+"/vendors").Result;
            HttpResponseMessage responseGetProducts = httpClient.GetAsync(DataServiceHelper.baseRestUrl + "/products").Result;
            HttpResponseMessage responseGetPurchase = httpClient.GetAsync(DataServiceHelper.baseRestUrl + "/purchaseOrders/" + this.purchaseId).Result;

            var vendors = responseGetVendors.Content.ReadAsAsync<IEnumerable<Vendor>>().Result;
            var vendorsList = vendors.ToList();
            var comboLabel = new Vendor { id=0, name = "- Select -" };
            vendorsList.Insert(0, comboLabel);
            this.vendorCombo.DataSource = vendorsList;

            var products = responseGetProducts.Content.ReadAsAsync<IEnumerable<Product>>().Result;
            this.products = products.ToList();
            var productLabel = new Product { id = 0, name = "- Select -" };
            this.products.Insert(0, productLabel);

            DataGridViewComboBoxColumn colbox = (DataGridViewComboBoxColumn) this.tableOrderDetails.Columns[0];
            colbox.DataSource = this.products;
            colbox.DisplayMember = "name";
            colbox.ValueMember = "id";

            List<MeasurementSelect> selects = new List<MeasurementSelect>();
            selects.Add(new MeasurementSelect { name = "Standard", isPurchaseUOM = false });
            selects.Add(new MeasurementSelect { name = "Purchase", isPurchaseUOM = true });
            DataGridViewComboBoxColumn colUOM = (DataGridViewComboBoxColumn)this.tableOrderDetails.Columns[1];
            colUOM.DisplayMember = "name";
            colUOM.ValueMember = "isPurchaseUOM";
            colUOM.DataSource = selects;
            colUOM.DataPropertyName = "usePurchaseUOM";

            this.purchase = responseGetPurchase.Content.ReadAsAsync<Purchase>().Result;
            setOrderDetailsStringValues(this.purchase.orders);
            this.orderDetailsList = this.purchase.orders;
            vendorCombo.SelectedValue = this.purchase.vendor.id;
            dtDate.Value = this.purchase.date;
            dtDueDate.Value = this.purchase.dueDate;
            txtNo.Text = this.purchase.no.ToString();
            setTotal();

            var gridBindingList = new BindingList<OrderDetails>(this.orderDetailsList);
            tableOrderDetails.AutoGenerateColumns = false;
            tableOrderDetails.DataSource = gridBindingList;

        }

        private void setOrderDetailsStringValues(List<OrderDetails> orderDetailsList)
        {
            foreach(OrderDetails orderDetails in orderDetailsList)
            {
                orderDetails.quantityUOMStr = DataServiceHelper.numberToString(orderDetails.quantityUOM);
                orderDetails.unitPriceStr = DataServiceHelper.numberToString(orderDetails.unitPrice);
                orderDetails.discountStr = DataServiceHelper.numberToString(orderDetails.discount);
                orderDetails.subTotalStr = DataServiceHelper.numberToString(orderDetails.subTotal);
                orderDetails.productId = (long) orderDetails.product.id;
            }
        }

        private void setTableMeasurementSelect(List<OrderDetails> orderDetailsList,int rowIndex)
        {
            if (rowIndex >= 0 && rowIndex < orderDetailsList.Count())
            {

                OrderDetails orderDetails = orderDetailsList[rowIndex];
                Product product = orderDetails.product;
                if (product != null)
                {

                    string standardUOM = "Standard ( " + product.standardUOM.name + " )";
                    string purchaseUOM = "Purchase ( " + product.purchaseUOM.name + " )";

                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)tableOrderDetails.Rows[rowIndex].Cells["colMeasurement"];
                    List<MeasurementSelect> selects = new List<MeasurementSelect>();
                    selects.Add(new MeasurementSelect { name = purchaseUOM, isPurchaseUOM = true });
                    selects.Add(new MeasurementSelect { name = standardUOM, isPurchaseUOM = false });

                    cell.DataSource = selects;
                }
            }
        }

        private Product getProductById(List<Product> products, long productId)
        {
            Product ret = null;
            foreach (Product product in products)
            {
                if(product.id==productId)
                {
                    return product;
                }
            }
            return ret;
        }

        private void tableOrderDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0)
            {
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)tableOrderDetails.Rows[e.RowIndex].Cells["colMeasurement"];
                List<MeasurementSelect> selects = new List<MeasurementSelect>();

                long productId = this.orderDetailsList[e.RowIndex].productId;
                Product product = getProductById(this.products, productId);

                string standardUOM = "Standard ( " + product.standardUOM.name + " )";
                string purchaseUOM = "Purchase ( " + product.purchaseUOM.name + " )";

                selects.Add(new MeasurementSelect { name = purchaseUOM, isPurchaseUOM = true });
                selects.Add(new MeasurementSelect { name = standardUOM, isPurchaseUOM = false });
                
                cell.DataSource = selects;
                cell.DisplayMember = "name";
                cell.ValueMember = "isPurchaseUOM";
            }
            else if (
                    e.RowIndex >= 0 &&
                    (
                        e.ColumnIndex == 2 || e.ColumnIndex == 3 || e.ColumnIndex == 4
                    )
                )
            {
                int rowIndex = e.RowIndex;
                DataGridViewTextBoxCell quantityCell = (DataGridViewTextBoxCell)tableOrderDetails.Rows[rowIndex].Cells["colQuantityUOM"];
                DataGridViewTextBoxCell unitPriceCell = (DataGridViewTextBoxCell)tableOrderDetails.Rows[rowIndex].Cells["colUnitPrice"];
                DataGridViewTextBoxCell discountCell = (DataGridViewTextBoxCell)tableOrderDetails.Rows[rowIndex].Cells["colDiscount"];
                DataGridViewTextBoxCell subTotalCell = (DataGridViewTextBoxCell)tableOrderDetails.Rows[rowIndex].Cells["colSubTotal"];

                string quantityUOMStr = quantityCell.Value == null ? "0" : quantityCell.Value.ToString();
                string unitPriceStr = unitPriceCell.Value == null ? "0" : unitPriceCell.Value.ToString();
                string discountStr = discountCell.Value == null ? "0" : discountCell.Value.ToString();
                string subTotalStr = subTotalCell.Value == null ? "0" : subTotalCell.Value.ToString();

                double quantityUOM = DataServiceHelper.tryParseNumber(quantityUOMStr) ? DataServiceHelper.stringToNumber(quantityUOMStr) : 0;
                double unitPrice = DataServiceHelper.tryParseNumber(unitPriceStr) ? DataServiceHelper.stringToNumber(unitPriceStr) : 0;
                double discount = DataServiceHelper.tryParseNumber(discountStr) ? DataServiceHelper.stringToNumber(discountStr) : 0;
                double subTotal = quantityUOM * unitPrice;
                subTotal -= (discount / 100.00) * subTotal;

                quantityUOMStr = DataServiceHelper.numberToString(quantityUOM);
                unitPriceStr = DataServiceHelper.numberToString(unitPrice);
                discountStr = DataServiceHelper.numberToString(discount);
                subTotalStr = DataServiceHelper.numberToString(subTotal);

                quantityCell.Value = quantityUOMStr;
                unitPriceCell.Value = unitPriceStr;
                discountCell.Value = discountStr;
                subTotalCell.Value = subTotalStr;
            }

            setTotal();
        }

        private void setTotal()
        {
            double taxPercent = this.taxPercent;
            double taxTotal = this.taxTotal;
            double freight = this.freight;
            double total = this.total;

            double subTotals = 0;

            foreach(OrderDetails orderDetails in this.orderDetailsList)
            {
                orderDetails.quantityUOMStr = orderDetails.quantityUOMStr == null ? "0" : orderDetails.quantityUOMStr;
                orderDetails.unitPriceStr = orderDetails.unitPriceStr == null ? "0" : orderDetails.unitPriceStr;
                orderDetails.discountStr = orderDetails.discountStr == null ? "0" : orderDetails.discountStr;
                orderDetails.subTotalStr = orderDetails.subTotalStr == null ? "0" : orderDetails.subTotalStr;
                orderDetails.purchaseUOMConversion = orderDetails.purchaseUOMConversion == null ? 0 : orderDetails.purchaseUOMConversion;

                string quantityUOMStr = orderDetails.quantityUOMStr;
                string unitPriceStr = orderDetails.unitPriceStr;
                string discountStr = orderDetails.discountStr;
                string subTotalStr = orderDetails.subTotalStr;

                double quantityUOM = DataServiceHelper.stringToNumber(orderDetails.quantityUOMStr); 
                double unitPrice = DataServiceHelper.stringToNumber(orderDetails.unitPriceStr);
                double discount = DataServiceHelper.stringToNumber(orderDetails.discountStr);
                double subTotal = orderDetails.quantityUOM * orderDetails.unitPrice;
                double purchaseUOMConversion = orderDetails.purchaseUOMConversion;
                subTotal -= (discount / 100.00) * subTotal;

                orderDetails.quantityUOM = quantityUOM;
                orderDetails.quantity = quantityUOM * purchaseUOMConversion;
                orderDetails.unitPrice = unitPrice;
                orderDetails.discount = discount;
                orderDetails.subTotal = subTotal;

                subTotals += subTotal;
            }

            taxTotal = (taxPercent / 100.0) * subTotals;

            total = subTotals + taxTotal + freight;

            this.taxTotal = taxTotal;
            this.taxTotalStr = DataServiceHelper.numberToString(taxTotal);

            this.subTotals = subTotals;
            this.subTotalsStr = DataServiceHelper.numberToString(subTotals);

            this.total = total;
            this.totalStr = DataServiceHelper.numberToString(total);

            txtSubTotal.Text = this.subTotalsStr;
            txtTaxTotal.Text = this.taxTotalStr;
            txtTotal.Text = this.totalStr;
        }

        private Product getProductById(long productId, List<Product> products)
        {
            Product ret = null;
            foreach(Product product in products)
            {
                if(product.id==productId)
                {
                    return product;
                }
            }
            return ret;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<OrderDetails> orderDetailsList = this.orderDetailsList;
            foreach(OrderDetails orderDetails in orderDetailsList) {
                Product product = getProductById(orderDetails.productId, this.products);
                double purchaseConversionUOM = orderDetails.usePurchaseUOM ? product.purchaseUOMConversion : 1;

                orderDetails.product = product;
                orderDetails.purchaseUOMConversion = purchaseConversionUOM;
            }

            this.purchase.no = txtNo.Text;
            this.purchase.date = dtDate.Value.Date;
            this.purchase.dueDate = dtDueDate.Value.Date;
            this.purchase.taxPercent = this.taxPercent;
            this.purchase.freight = this.freight;
            this.purchase.orders = orderDetailsList;
            this.purchase.vendor = new Vendor { id = Int64.Parse(this.vendorCombo.SelectedValue.ToString()) };

            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage response = httpClient.PutAsJsonAsync(DataServiceHelper.baseRestUrl + "purchaseOrders/" + purchase.id, this.purchase).Result;

            var result = MessageBox.Show("Purchase update success!", "Success",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                Close();
            }
        }

        private void tableOrderDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            int columnIndex = dg.CurrentCell.ColumnIndex;
            if (columnIndex == 2 || columnIndex == 3 || columnIndex == 4)
            {
                DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
                tb.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
                e.Control.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
            }
        }

        private void dataGridViewTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"^[0-9|\.|\,]+$")
                && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtTotal_TextChanged(object sender, EventArgs e)
        {
            txtTaxPercent.Text = DataServiceHelper.tryParseNumber(txtTaxPercent.Text) ? txtTaxPercent.Text : "0";
            this.taxPercentStr = txtTaxPercent.Text;
            this.taxPercent = DataServiceHelper.stringToNumber(this.taxPercentStr);

            txtFreight.Text = DataServiceHelper.tryParseNumber(txtFreight.Text) ? txtFreight.Text : "0";
            this.freightStr = txtFreight.Text;
            this.freight = DataServiceHelper.stringToNumber(this.freightStr);

            setTotal();
        }

        private void txtTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"^[0-9|\.|\,]+$")
                && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tableOrderDetails_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            setTableMeasurementSelect(this.orderDetailsList,e.RowIndex);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tableOrderDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.ColumnIndex == 6 &&
                e.RowIndex >= 0)
            {
                int index = e.RowIndex;

                var result = MessageBox.Show("Are you sure to delete this row!", "Delete",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    tableOrderDetails.Rows.RemoveAt(index);
                }
            }
        }
    }
}
