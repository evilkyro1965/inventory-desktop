﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using kyro.inventory.model;
using kyro.inventory.service;
using Newtonsoft.Json;

namespace kyro.inventory.form
{
    public partial class FormSales : Form
    {
        BindingSource bindingSource;

        public List<SalesDetails> salesDetailsList = new List<SalesDetails>();
        public List<Product> products;
        public List<Location> locations;
        public double subTotals = 0;
        public string subTotalsStr = "0";
        public double taxPercent = 0;
        public string taxPercentStr = "0";
        public double taxTotal = 0;
        public string taxTotalStr = "0";
        public double freight = 0;
        public string freightStr = "0";
        public double total = 0;
        public string totalStr = "0";

        public FormSales()
        {
            InitializeComponent();

            this.tableOrderDetails.Columns["colQuantityUOM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.tableOrderDetails.Columns["colUnitPrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.tableOrderDetails.Columns["colDiscount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.tableOrderDetails.Columns["colSubTotal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            txtFreight.Text = "0";
            txtTaxPercent.Text = "0";
        }

        private void FormPurchaseOrder_Load(object sender, EventArgs e)
        {
            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage responseGetCustomers = httpClient.GetAsync(DataServiceHelper.baseRestUrl+"/customers").Result;
            HttpResponseMessage responseGetProducts = httpClient.GetAsync(DataServiceHelper.baseRestUrl + "/products").Result;
            HttpResponseMessage responseGetLocation = httpClient.GetAsync(DataServiceHelper.baseRestUrl + "/locations/").Result;

            var customers = responseGetCustomers.Content.ReadAsAsync<IEnumerable<Customer>>().Result;
            var customerList = customers.ToList();
            var comboLabel = new Customer { id=0, name = "- Select -" };
            customerList.Insert(0, comboLabel);
            this.customerCombo.DataSource = customerList;

            var products = responseGetProducts.Content.ReadAsAsync<IEnumerable<Product>>().Result;
            this.products = products.ToList();
            var productLabel = new Product { id = 0, name = "- Select -" };
            this.products.Insert(0, productLabel);

            var locations= responseGetLocation.Content.ReadAsAsync<IEnumerable<Location>>().Result;
            this.locations = locations.ToList();
            var locationDefault = new Location { id = 0, name = "- Select -" };
            this.locations.Insert(0, locationDefault);
            this.locationCombo.DataSource = locations;

            DataGridViewComboBoxColumn colbox = (DataGridViewComboBoxColumn) this.tableOrderDetails.Columns[0];
            colbox.DataSource = this.products;
            colbox.DisplayMember = "name";
            colbox.ValueMember = "id";

            List<MeasurementSelect> selects = new List<MeasurementSelect>();
            selects.Add(new MeasurementSelect { name = "Standard", isPurchaseUOM = false });
            selects.Add(new MeasurementSelect { name = "Purchase", isPurchaseUOM = true });
            DataGridViewComboBoxColumn colUOM = (DataGridViewComboBoxColumn)this.tableOrderDetails.Columns[1];
            colUOM.DataSource = selects;
            colUOM.DisplayMember = "name";
            colUOM.ValueMember = "isPurchaseUOM";

            var gridBindingList = new BindingList<SalesDetails>(this.salesDetailsList);
            tableOrderDetails.AutoGenerateColumns = false;
            tableOrderDetails.DataSource = gridBindingList;
        }

        private Product getProductById(long productId, List<Product> products)
        {
            Product ret = null;
            foreach (Product product in products)
            {
                if(product.id==productId)
                {
                    return product;
                }
            }
            return ret;
        }

        private void tableOrderDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex >= 0)
            {
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)tableOrderDetails.Rows[e.RowIndex].Cells["colMeasurement"];
                List<MeasurementSelect> selects = new List<MeasurementSelect>();

                long productId = this.salesDetailsList[e.RowIndex].productId;
                Product product = getProductById(productId, this.products);

                string standardUOM = "Standard ( " + product.standardUOM.name + " )";
                string purchaseUOM = "Purchase ( " + product.purchaseUOM.name + " )";

                selects.Add(new MeasurementSelect { name = purchaseUOM, isPurchaseUOM = true });
                selects.Add(new MeasurementSelect { name = standardUOM, isPurchaseUOM = false });
                
                cell.DataSource = selects;
                cell.DisplayMember = "name";
                cell.ValueMember = "isPurchaseUOM";
            }
            else if (
                    e.RowIndex >= 0 && 
                    (
                        e.ColumnIndex == 2 || e.ColumnIndex == 3 || e.ColumnIndex == 4
                    )
                )
            {
                int rowIndex = e.RowIndex;
                DataGridViewTextBoxCell quantityCell = (DataGridViewTextBoxCell) tableOrderDetails.Rows[rowIndex].Cells["colQuantityUOM"];
                DataGridViewTextBoxCell unitPriceCell = (DataGridViewTextBoxCell) tableOrderDetails.Rows[rowIndex].Cells["colUnitPrice"];
                DataGridViewTextBoxCell discountCell = (DataGridViewTextBoxCell) tableOrderDetails.Rows[rowIndex].Cells["colDiscount"];
                DataGridViewTextBoxCell subTotalCell = (DataGridViewTextBoxCell) tableOrderDetails.Rows[rowIndex].Cells["colSubTotal"];

                string quantityUOMStr = quantityCell.Value == null ? "0" : quantityCell.Value.ToString();
                string unitPriceStr = unitPriceCell.Value == null ? "0" : unitPriceCell.Value.ToString();
                string discountStr = discountCell.Value == null ? "0" : discountCell.Value.ToString();
                string subTotalStr = subTotalCell.Value == null ? "0" : subTotalCell.Value.ToString();

                double quantityUOM = DataServiceHelper.tryParseNumber(quantityUOMStr) ? DataServiceHelper.stringToNumber(quantityUOMStr) : 0;
                double unitPrice = DataServiceHelper.tryParseNumber(unitPriceStr) ? DataServiceHelper.stringToNumber(unitPriceStr) : 0;
                double discount = DataServiceHelper.tryParseNumber(discountStr) ? DataServiceHelper.stringToNumber(discountStr) : 0;
                double subTotal = quantityUOM * unitPrice;
                subTotal -= (discount / 100.00) * subTotal;

                quantityUOMStr = DataServiceHelper.numberToString(quantityUOM);
                unitPriceStr = DataServiceHelper.numberToString(unitPrice);
                discountStr = DataServiceHelper.numberToString(discount);
                subTotalStr = DataServiceHelper.numberToString(subTotal);

                quantityCell.Value = quantityUOMStr;
                unitPriceCell.Value = unitPriceStr;
                discountCell.Value = discountStr;
                subTotalCell.Value = subTotalStr;
            }

            setTotal();
        }

        private void setTotal()
        {
            double taxPercent = this.taxPercent;
            double taxTotal = this.taxTotal;
            double freight = this.freight;
            double total = this.total;

            double subTotals = 0;

            foreach(SalesDetails salesDetails in this.salesDetailsList)
            {
                salesDetails.quantityUOMStr = salesDetails.quantityUOMStr == null ? "0" : salesDetails.quantityUOMStr;
                salesDetails.unitPriceStr = salesDetails.unitPriceStr == null ? "0" : salesDetails.unitPriceStr;
                salesDetails.discountStr = salesDetails.discountStr == null ? "0" : salesDetails.discountStr;
                salesDetails.subTotalStr = salesDetails.subTotalStr == null ? "0" : salesDetails.subTotalStr;
                salesDetails.salesUOMConversion = salesDetails.salesUOMConversion == null ? 0 : salesDetails.salesUOMConversion;

                string quantityUOMStr = salesDetails.quantityUOMStr;
                string unitPriceStr = salesDetails.unitPriceStr;
                string discountStr = salesDetails.discountStr;
                string subTotalStr = salesDetails.subTotalStr;

                double quantityUOM = DataServiceHelper.stringToNumber(salesDetails.quantityUOMStr); 
                double unitPrice = DataServiceHelper.stringToNumber(salesDetails.unitPriceStr);
                double discount = DataServiceHelper.stringToNumber(salesDetails.discountStr);
                double subTotal = salesDetails.quantityUOM * salesDetails.unitPrice;
                double purchaseUOMConversion = salesDetails.salesUOMConversion;
                subTotal -= (discount / 100.00) * subTotal;

                salesDetails.quantityUOM = quantityUOM;
                salesDetails.quantity = quantityUOM * purchaseUOMConversion;
                salesDetails.unitPrice = unitPrice;
                salesDetails.discount = discount;
                salesDetails.subTotal = subTotal;

                subTotals += subTotal;
            }

            taxTotal = (taxPercent / 100.0) * subTotals;

            total = subTotals + taxTotal + freight;

            this.taxTotal = taxTotal;
            this.taxTotalStr = DataServiceHelper.numberToString(taxTotal);

            this.subTotals = subTotals;
            this.subTotalsStr = DataServiceHelper.numberToString(subTotals);

            this.total = total;
            this.totalStr = DataServiceHelper.numberToString(total);

            txtSubTotal.Text = this.subTotalsStr;
            txtTaxTotal.Text = this.taxTotalStr;
            txtTotal.Text = this.totalStr;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<SalesDetails> salesDetailsList = this.salesDetailsList;
            foreach(SalesDetails salesDetails in salesDetailsList) {
                Product product = getProductById(salesDetails.productId, this.products);
                double salesConversionUOM = salesDetails.useSalesUOM ? product.salesUOMConversion : 1;

                salesDetails.product = product;
                salesDetails.salesUOMConversion = salesConversionUOM;
            }

            Sales sales = new Sales();
            sales.no = txtNo.Text;
            sales.date = dtDate.Value.Date;
            sales.dueDate = dtDueDate.Value.Date;
            sales.taxPercent = this.taxPercent;
            sales.freight = this.freight;
            sales.salesList = salesDetailsList;
            sales.location = new Location { id = Int64.Parse(this.locationCombo.SelectedValue.ToString()) };
            sales.customer = new Customer { id = Int64.Parse(this.customerCombo.SelectedValue.ToString()) };

            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage response = httpClient.PostAsJsonAsync(DataServiceHelper.baseRestUrl + "sales", sales).Result;
            var result = MessageBox.Show("Purchase add success!", "Success",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                Close();
            }
        }

        private void tableOrderDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            int columnIndex = dg.CurrentCell.ColumnIndex;
            if (columnIndex == 2 || columnIndex == 3 || columnIndex == 4)
            {
                DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
                tb.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
                e.Control.KeyPress += new KeyPressEventHandler(dataGridViewTextBox_KeyPress);
            }
        }

        private void dataGridViewTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"^[0-9|\.|\,]+$")
                && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtTotal_TextChanged(object sender, EventArgs e)
        {
            txtTaxPercent.Text = DataServiceHelper.tryParseNumber(txtTaxPercent.Text) ? txtTaxPercent.Text : "0";
            this.taxPercentStr = txtTaxPercent.Text;
            this.taxPercent = DataServiceHelper.stringToNumber(this.taxPercentStr);

            txtFreight.Text = DataServiceHelper.tryParseNumber(txtFreight.Text) ? txtFreight.Text : "0";
            this.freightStr = txtFreight.Text;
            this.freight = DataServiceHelper.stringToNumber(this.freightStr);

            setTotal();
        }

        private void txtTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"^[0-9|\.|\,]+$")
                && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tableOrderDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.ColumnIndex == 6 &&
                e.RowIndex >= 0)
            {
                int index = e.RowIndex;

                var result = MessageBox.Show("Are you sure to delete this row!", "Delete",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    tableOrderDetails.Rows.RemoveAt(index);
                }
            }
        }
    }
}
