﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using kyro.inventory.model;
using kyro.inventory.service;
using System.Reflection;

namespace kyro.inventory.form
{
    public partial class FormPurchases : Form
    {
        public PurchaseSearchCriteria criteria = new PurchaseSearchCriteria();
        public List<Purchase> purchases;


        public FormPurchases()
        {
            InitializeComponent();
        }

        private void FormPurchases_Load(object sender, EventArgs e)
        {
            HttpClient httpClient = DataServiceHelper.getHttpClient();
            HttpResponseMessage responseGetPurchase = httpClient.PostAsJsonAsync(DataServiceHelper.baseRestUrl + "/purchaseOrders/search", this.criteria).Result;

            HttpHeaders headers = responseGetPurchase.Headers;
            IEnumerable<string> values;
            if (headers.TryGetValues("X-Total-Count", out values))
            {
                this.criteria.totalRow = Int32.Parse(values.First());
            }
            if (headers.TryGetValues("X-Total-Page", out values))
            {
                this.criteria.page = Int32.Parse(values.First());
            }

            var purchases = responseGetPurchase.Content.ReadAsAsync<IEnumerable<Purchase>>().Result;
            this.purchases = purchases.ToList();

            tablePurchase.Columns["dateColumn"].DefaultCellStyle.Format = "dd/MM/yyyy";
            tablePurchase.Columns["dueDateColumn"].DefaultCellStyle.Format = "dd/MM/yyyy";

            var gridBindingList = new BindingList<Purchase>(this.purchases);
            tablePurchase.AutoGenerateColumns = false;
            tablePurchase.DataSource = gridBindingList;
        }

        private void tablePurchase_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView grid = (DataGridView)sender;
            DataGridViewRow row = grid.Rows[e.RowIndex];
            DataGridViewColumn col = grid.Columns[e.ColumnIndex];

            if(col.Index==4 && e.Value != null)
            {
                double d = double.Parse(e.Value.ToString());
                e.Value = DataServiceHelper.numberToString(d);
            }

            if (row.DataBoundItem != null && col.DataPropertyName.Contains("."))
            {
                string[] props = col.DataPropertyName.Split('.');
                PropertyInfo propInfo = row.DataBoundItem.GetType().GetProperty(props[0]);
                object val = propInfo.GetValue(row.DataBoundItem, null);
                for (int i = 1; i < props.Length; i++)
                {
                    propInfo = val.GetType().GetProperty(props[i]);
                    val = propInfo.GetValue(val, null);
                }
                e.Value = val;
            }
        }

        private void tablePurchase_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.ColumnIndex == 5 &&
                e.RowIndex >= 0)
            {
                long purchaseId = Int64.Parse(this.purchases[e.RowIndex].id.ToString());
                FormPurchaseOrderUpdate formUpdate = new FormPurchaseOrderUpdate(purchaseId);
                formUpdate.MdiParent = this.ParentForm;
                formUpdate.Show();
                Close();
            }
            else if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.ColumnIndex == 6 &&
                e.RowIndex >= 0)
            {
                long purchaseId = Int64.Parse(this.purchases[e.RowIndex].id.ToString());
                FormPurchaseOrderReceive formUpdate = new FormPurchaseOrderReceive(purchaseId);
                formUpdate.MdiParent = this.ParentForm;
                formUpdate.Show();
                Close();
            }
            else if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.ColumnIndex == 7 &&
                e.RowIndex >= 0)
            {
                long purchaseId = Int64.Parse(this.purchases[e.RowIndex].id.ToString());
                FormPurchaseOrderReturn formUpdate = new FormPurchaseOrderReturn(purchaseId);
                formUpdate.MdiParent = this.ParentForm;
                formUpdate.Show();
                Close();
            }
        }
    }
}
