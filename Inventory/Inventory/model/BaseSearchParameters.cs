﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kyro.inventory.model
{
    public class BaseSearchParameters
    {
        public int page { get; set; }

        public int limit { get; set; }

        public string order { get; set; }

        public long totalRow { get; set; }

        public long totalPage { get; set; }

        public string name { get; set; }

        public BaseSearchParameters()
        {
            page = 1;
            limit = 1;
            order = "asc";
        }
    }
}
