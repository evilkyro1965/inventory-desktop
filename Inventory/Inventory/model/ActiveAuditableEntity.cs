﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kyro.inventory.model
{
    public class ActiveAuditableEntity : AuditableEntity
    {
        public bool isActive { get; set; }
    }
}
