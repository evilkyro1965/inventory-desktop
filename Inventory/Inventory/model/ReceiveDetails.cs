﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace kyro.inventory.model
{
    public class ReceiveDetails : IdentifiableEntity
    {
        public long? purchaseId { get; set; }
        public Product product { get; set; }
        public Location location { get; set; }
        public double quantityUOM { get; set; }
        public string quantityUOMStr { get; set; }
        public double quantity { get; set; }
        public bool usePurchaseUOM { get; set; }
        public double purchaseUOMConversion { get; set; }
        public DateTime receiveDate { get; set; }
        public long? locationId { get; set; }
        [JsonIgnore]
        public OrderDetails orderDetails { get; set; }
        [JsonIgnore]
        public string orderQtyStr { get; set; }
    }
}
