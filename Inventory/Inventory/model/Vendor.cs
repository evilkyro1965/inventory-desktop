﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Converters;

namespace kyro.inventory.model
{
    public class Vendor : ActiveAuditableEntity
    {

    }

    public class VendorConverter : CustomCreationConverter<Vendor>
    {
        public override Vendor Create(Type objectType)
        {
            var vendor = new Vendor();
            vendor.createdDate = new DateTime();
            Console.WriteLine("hi");
            return vendor;
        }
    }
}
