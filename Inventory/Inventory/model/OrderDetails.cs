﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kyro.inventory.model
{
    public class OrderDetails : IdentifiableEntity
    {
        public Product product { get; set; }
        public double quantityUOM { get; set; }
        public string quantityUOMStr { get; set; }
        public double quantity { get; set; }
        public bool usePurchaseUOM { get; set; }
        public double purchaseUOMConversion { get; set; }
        public double unitPrice { get; set; }
        public string unitPriceStr { get; set; }
        public double totalPrice { get; set; }
        public double totalPriceStr { get; set; }
        public double discount { get; set; }
        public string discountStr { get; set; }
        public double discountTotal { get; set; }
        public double subTotal { get; set; }
        public string subTotalStr { get; set; }
        public DateTime date { get; set; }
        public long productId { get; set; }
        public ReceiveDetails receiveDetails { get; set; }
        public ReturnDetails returnDetails { get; set; }


    }
}
