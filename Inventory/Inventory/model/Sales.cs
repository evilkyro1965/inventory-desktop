﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kyro.inventory.model
{
    public class Sales
    {
        public string no { get; set; }
        public DateTime date { get; set; }
        public DateTime dueDate { get; set; }
        public Customer customer { get; set; }
        public Location location { get; set; }
        public double taxPercent { get; set; }
        public double freight { get; set; }
        public List<SalesDetails> salesList { get; set; }
        public Boolean paid { get; set; }
        public Boolean closed { get; set; }
        public double total { get; set; }
    }
}
