﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kyro.inventory.model
{
    public class Product : ActiveAuditableEntity
    {
        public string code { get; set; }
        public string barcode { get; set; }
        public Measurement standardUOM { get; set; }
        public Measurement salesUOM { get; set; }
        public double salesUOMConversion { get; set; }
        public Measurement purchaseUOM { get; set; }
        public double purchaseUOMConversion { get; set; }
    }
}
