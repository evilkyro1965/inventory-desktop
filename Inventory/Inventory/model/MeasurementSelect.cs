﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kyro.inventory.model
{
    public class MeasurementSelect
    {
        public string name { get; set; }
        public bool isPurchaseUOM { get; set; }
    }
}
