﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kyro.inventory.model
{
    public class Purchase : IdentifiableEntity
    {
        public string no { get; set; }
        public DateTime date { get; set; }
        public DateTime dueDate { get; set; }
        public Vendor vendor { get; set; }
        public double taxPercent { get; set; }
        public double freight { get; set; }
        public List<OrderDetails> orders { get; set; }
        public Boolean paid { get; set; }
        public Boolean closed { get; set; }
        public Boolean fulfilled { get; set; }
        public double total { get; set; }
    }
}
